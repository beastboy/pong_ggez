use ggez::{conf::WindowSetup, event::{self, EventHandler}};
use ggez::graphics;
use ggez::graphics::clear;
use ggez::graphics::present;
use ggez::graphics::Color;
use ggez::graphics::Rect;
use ggez::{Context, ContextBuilder, GameResult};
use ggez::input::keyboard::is_key_pressed;
use ggez::input::keyboard::KeyCode;
type Vector = ggez::mint::Vector2<f32>;

// taille d'ecran
const SCREEN_HEIGHT: f32 = 600.0;
const SCREEN_WIDTH: f32 = 600.0;

// vitesse 
const SPEED : f32 = 8.0;

// taille des joueur
const PLAYER_WIDTH: f32 = 12.;
const PLAYER_HEIGHT: f32 = 75.;

const BALL_RADIUS: f32 = 10.;

struct MainState {
    l_player: Rect,
    r_player: Rect,
    ball: Ball,
    l_score: u8,
    r_score: u8,
}

struct Ball {
    rect: Rect,
    vel: Vector,
}

impl Ball {
    fn new() -> Self{
        use rand::{thread_rng, Rng};
        let mut rng = thread_rng();
        let mut x = rng.gen_range(3.0,5.0);
        let mut y = rng.gen_range(3.0,5.0);

        if rng.gen::<bool>() {
            x *= -1.0;
        }
        if rng.gen::<bool>() {
            y *= -1.0;
        }
        Ball {
            rect: Rect::new(
                SCREEN_WIDTH / 2.0 - BALL_RADIUS / 2.0,
                SCREEN_HEIGHT / 2.0 - BALL_RADIUS / 2.0,
                BALL_RADIUS,
                BALL_RADIUS,
            ),
            vel: Vector { x, y },
        }
    }
}
impl EventHandler for MainState {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        if is_key_pressed(ctx, KeyCode::S){
            self.l_player.y -= SPEED
        }
        if is_key_pressed(ctx, KeyCode::W){
            self.l_player.y += SPEED
        }
        if is_key_pressed(ctx, KeyCode::Up){
            self.r_player.y -= SPEED
        }
        if is_key_pressed(ctx, KeyCode::Down){
            self.r_player.y += SPEED
        }

        self.ball.rect.translate(self.ball.vel);    

        if self.ball.vel.x < 0.0 &&  self.ball.rect.overlaps(&self.l_player) || self.ball.vel.x > 0.0 &&  self.ball.rect.overlaps(&self.r_player){
            self.ball.vel.x *= -1.0;
        }
        if (self.ball.vel.y < 0.0 && self.ball.rect.top() < 0.0)
        || (self.ball.vel.y > 0.0 && self.ball.rect.bottom() > SCREEN_HEIGHT) {
            self.ball.vel.y *= -1.0;
        }
        if self.ball.rect.left() < 0.0{
            self.r_score += 1;
            std::thread::sleep(std::time::Duration::from_millis(1000));
            self.ball = Ball::new();
        }
        if self.ball.rect.right() > SCREEN_WIDTH{
            self.l_score += 1;
            std::thread::sleep(std::time::Duration::from_millis(1000));
            self.ball = Ball::new();
        }
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        clear(ctx, Color::new(0.0, 0.0, 0.0, 1.0));
        let ball_mesh = graphics::Mesh::new_rectangle(  
            ctx,
            graphics::DrawMode::fill(),
            self.ball.rect,
            Color::new(1.0, 1.0, 1.0, 1.0),
        )
        .unwrap();
        graphics::draw(ctx, &ball_mesh, graphics::DrawParam::default()).unwrap();
        // joueur gauche
        let r_player_mesh = graphics::Mesh::new_rectangle(
            ctx,
            graphics::DrawMode::stroke(1.0),
            self.r_player,
            Color::new(1.0, 1.0, 0.5, 1.0),
        ).unwrap();
        graphics::draw(ctx,&r_player_mesh,graphics::DrawParam::default()).unwrap();
        // joueur droit

        let l_player_mesh = graphics::Mesh::new_rectangle(
            ctx,
            graphics::DrawMode::stroke(1.0),
            self.l_player,
            Color::new(1.0, 0.5, 1.0, 1.0),
        ).unwrap();
        graphics::draw(ctx,&l_player_mesh,graphics::DrawParam::default()).unwrap();

        let score = graphics::Text::new(format!("L : {} \t R : {}",self.l_score,self.r_score));
        let coord = [SCREEN_WIDTH /2.0 - score.width(ctx) as f32 /2.0,50.0];
        let params = graphics::DrawParam::default().dest(coord);
        graphics::draw(ctx, &score, params).expect("error drawing scoreboard text");

        present(ctx).unwrap();
        Ok(())
    }
}

fn main() -> GameResult {
    let (ctx, event_loop) = &mut ContextBuilder::new("Pong", "Beastboy")
    .window_mode(ggez::conf::WindowMode::default().dimensions(SCREEN_WIDTH,SCREEN_HEIGHT))
    .window_setup(WindowSetup::default().title("Pong"))
    .build().unwrap();
    // reference mutable a la struct 'MainState'
    let main_state = &mut MainState {
        l_player: Rect::new(
            50.0,
            SCREEN_HEIGHT / 2.0 - PLAYER_HEIGHT / 2.0,
            PLAYER_WIDTH,
            PLAYER_HEIGHT,
        ),
        r_player: Rect::new(
            SCREEN_WIDTH - 50.0,
            SCREEN_HEIGHT / 2.0 - PLAYER_HEIGHT / 2.0,
            PLAYER_WIDTH,
            PLAYER_HEIGHT,
        ),
        ball: Ball::new(),
        l_score: 0,
        r_score: 0,
    };
    // run le jeu (return un gameResult)
    event::run(ctx, event_loop, main_state)
}
